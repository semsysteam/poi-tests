﻿#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json
import logging
import os
import random
from typing import Any

import requests
import swagger_client
from dotenv import load_dotenv
from swagger_client import OntologyEntityAttributeView
from swagger_client.models.ontology_view import OntologyEntityView, OntologyView
from swagger_client.rest import ApiException

log = logging.getLogger(__name__)


def get_access_token(url, login, password):
    login_url = '{0}/token'.format(url)
    payload = 'grant_type=password&username={0}&password={1}'.format(
        login, password)
    headers = {'accept': '*/*',
               'Content-Type': 'application/x-www-form-urlencoded', }
    auth_resp = requests.post(
        url=login_url, headers=headers, data=payload, verify=False)
    if auth_resp.status_code == 200:
        jwt = json.loads(auth_resp.content)
        token = jwt['access_token']
        return token
    else:
        raise Exception('ERROR: {0}. Auth request failed! {1}'.format(
            auth_resp.status_code, auth_resp.text))


def get_attribute_by_name(entity, attribute_name, locale='ru') -> OntologyEntityAttributeView:
    if entity.to_dict().get('attributes'):
        for attribute in entity.to_dict().get('attributes'):
            if attribute.get('attribute_description'):
                attribute_description = attribute.get(
                    'attribute_description')
                if attribute_description.get('name'):
                    name = attribute_description.get('name')
                    if name.get('values'):
                        localized_value = name.get('values').get(locale)
                        if localized_value and localized_value == attribute_name:
                            return attribute


def get_attribute_value_by_name(entity, attribute_name, locale='ru') -> Any:
    attribute = get_attribute_by_name(
        entity=entity, attribute_name=attribute_name, locale=locale)
    if attribute:
        return attribute.get('value')


class LoadTest(object):
    def __init__(self, gun):
        # you'll be able to call gun's methods using this field:
        self.gun = gun
        dotenv_path = os.path.join(os.path.dirname(
            os.path.realpath(__file__)), ".env")
        load_dotenv(dotenv_path)
        self.base_api_url = os.environ.get("BASE_API_URL")
        self.base_isapi_url = os.environ.get("BASE_ISAPI_URL")
        self.login = os.environ.get("API_USER_NAME")
        self.password = os.environ.get("API_USER_PASSWORD")
        self.access_token = get_access_token(
            self.base_api_url, self.login, self.password)

        configuration = swagger_client.Configuration()
        configuration.verify_ssl = False
        configuration.host = self.base_api_url
        configuration.access_token = self.access_token

        api_client = swagger_client.ApiClient(configuration)
        api_client.default_headers = {
            'Accept': 'application/json; charset=utf-8', 'Connection': 'Close', 'Cache-Control': 'max-age=0'}
        self.ontology_api_instance = swagger_client.OntologyApi(api_client)
        self.security_api_instance = swagger_client.SecurityAdministrationApi(
            api_client)
        self.ontology_entity_api_instance = swagger_client.OntologyEntityApi(
            api_client)

        # ontologies start        
        self.default_view_ontology = None
        self.employees_ontology = None
        self.equipment_ontology = None
        self.kanban_columns_ontology = None
        self.kanban_panel_ontology = None
        self.left_side_bar_ontology = None
        self.nomenclature_ontology = None
        self.ontology_of_ontologies = None
        self.selected_ontology = None
        self.shift_daily_tasks_ontology = None
        self.states_ontology = None
        self.top_bar_ontology = None
        self.user_ontology = None
        self.viz_ontology_with_user_rights = None
        # ontologies end

        self.selected_ontology_id = None
        self.selected_card_entity_id = None
        self.bp_enity_id = None
        self.default_view_ontology_id = None
        self.defects_ontology_id = None
        self.employees_ontology_id = None
        self.equipment_ontology_id = None
        self.job_type_ontology_id = None
        self.kanban_columns_ontology_id = None
        self.kanban_panel_id = None
        self.nomenclature_ontology_id = None
        self.ontology_of_ontologies_id = None
        self.resolutions_ontology_id = None
        self.routes_ontology_id = None
        self.selected_card_entity = None
        self.shift_daily_tasks_ontology_id = None
        self.states_ontology_id = None
        self.user_id = self.get_current_user_id()

        # light ontologies start
        self.defects_light_ontology = None
        self.employees_light_ontology = None
        self.equipment_light_ontology = None
        self.job_type_light_ontology = None
        self.nomenclature_light_ontology = None
        self.resolutions_light_ontology = None
        self.routes_light_ontology = None
        self.states_light_ontology = None
        # light ontologies end

        self.bp_enity = None
        self.login_value = None
        self.ontology_of_ontologies_entity = None
        self.output_format_entity = None

        self.default_attribute_name = 'Наименование'

        self.get_ontology_of_ontologies()

    def get_ontology_id_by_name(self, ontology: OntologyView, ontology_name, locale='ru') -> Any:
        entities = ontology.to_dict().get('entities')
        for entity in entities:
            for attribute in entity.get('attributes'):
                attribute_description = attribute.get('attribute_description')
                if attribute_description.get('name').get('local_value') == self.default_attribute_name:
                    values = attribute.get('value').get('Values')
                    if values:
                        value_with_locale = values.get(locale)
                        if value_with_locale and value_with_locale == ontology_name:
                            return entity.get('id')

    def get_entity_id_by_name(self, entity: OntologyEntityView, entity_name, locale='ru') -> str:
        result = get_attribute_by_name(
            entity=entity, attribute_name=self.default_attribute_name, locale=locale)
        if result:
            value = result.get('value')
            if value == entity_name:
                return entity.get('id')

    def get_current_user_id(self) -> str:
        try:
            user_ontology = self.security_api_instance.security_administration_get_user_name()
            result = user_ontology.to_dict().get('id')
            return result
        except ApiException as exc:
            log.error(exc)
            raise

    def get_ontology_of_ontologies(self) -> OntologyView:
        try:
            self.ontology_of_ontologies_id = self.ontology_api_instance.ontology_get_main_ontology_id()
            if self.ontology_of_ontologies_id:
                self.ontology_of_ontologies = self.ontology_api_instance.ontology_get(
                    id=self.ontology_of_ontologies_id, load_attributes=True, load_visualisation=True, load_entities=True, load_binaries=False)
                if self.ontology_of_ontologies:
                    self.selected_ontology_id = self.get_ontology_id_by_name(self.ontology_of_ontologies,
                                                                             ontology_name='Сменно-суточные задания')

                    if self.selected_ontology_id:
                        self.selected_ontology = self.ontology_api_instance.ontology_get(
                            id=self.selected_ontology_id, load_attributes=True,
                            load_visualisation=True, load_entities=True, load_binaries=False)
                        if self.selected_ontology and self.selected_ontology.entities and len(self.selected_ontology.entities) > 0:
                            e_len = len(self.selected_ontology.entities)
                            self.shift_daily_tasks_ontology_id = self.selected_ontology_id
                            self.shift_daily_tasks_ontology = self.selected_ontology

                            self.bp_enity_id = get_attribute_value_by_name(
                                self.shift_daily_tasks_ontology, 'Канбан процесс')
                            self.kanban_panel_id = self.get_ontology_id_by_name(self.ontology_of_ontologies,
                                                                                ontology_name='Канбан панели')

                            self.default_view_ontology_id = self.get_ontology_id_by_name(self.ontology_of_ontologies,
                                                                                         ontology_name='Представление по умолчанию')
                            self.nomenclature_ontology_id = self.get_ontology_id_by_name(self.ontology_of_ontologies,
                                                                                         ontology_name='Номенклатура')
                            self.equipment_ontology_id = self.get_ontology_id_by_name(self.ontology_of_ontologies,
                                                                                      ontology_name='Оборудование')
                            self.employees_ontology_id = self.get_ontology_id_by_name(self.ontology_of_ontologies,
                                                                                      ontology_name='Сотрудники')
                            self.resolutions_ontology_id = self.get_ontology_id_by_name(self.ontology_of_ontologies,
                                                                                        ontology_name='Резолюции')
                            self.defects_ontology_id = self.get_ontology_id_by_name(self.ontology_of_ontologies,
                                                                                    ontology_name='Причина брака')
                            self.routes_ontology_id = self.get_ontology_id_by_name(self.ontology_of_ontologies,
                                                                                   ontology_name='Технологические маршруты')
                            self.states_ontology_id = self.get_ontology_id_by_name(self.ontology_of_ontologies,
                                                                                   ontology_name='Состояния')
                            self.job_type_ontology_id = self.get_ontology_id_by_name(self.ontology_of_ontologies,
                                                                                     ontology_name='Тип задания')
                            self.kanban_columns_ontology_id = self.get_ontology_id_by_name(self.ontology_of_ontologies,
                                                                                           ontology_name='Столбцы канбана')
                            i = random.randint(0, e_len - 1)
                            selected_entity = self.selected_ontology.entities[i]
                            if selected_entity and selected_entity.id:
                                self.selected_card_entity_id = selected_entity.id
                                self.selected_card_entity = self.ontology_entity_api_instance.ontology_entity_get(
                                    id=self.selected_card_entity_id)
                            else:
                                raise Exception(
                                    'selected_entity is None')
                        else:
                            raise Exception(
                                'self.selected_ontology or self.selected_ontology.entities is None')
                    else:
                        raise Exception(
                            'self.selected_ontology_id is None')
                else:
                    raise Exception(
                        'self.ontology_of_ontologies is None')
            else:
                raise Exception(
                    'ontology_of_ontologies_id is None')
        except Exception as exc:
            log.error(exc)
            raise

    # Возвращает сущность данного пользователя из онтологии пользователей #

    def case_1(self, missile):
        with self.gun.measure("case_1"):
            # GET <BASE_API_URL>/api/SecurityAdministration/currentUser
            log.info(
                "Shoot returns the entity of the \
                 given user from the user ontology step: %s", missile)
            try:
                self.user_ontology = self.security_api_instance.security_administration_get_user_name()
            except ApiException as exc:
                log.error(exc)
                raise

    # Загрузка онтологии LeftSideBar #

    def case_2(self, missile):
        with self.gun.measure("case_2"):
            # GET
            # <BASE_API_URL>/api/Ontology/ontologyBySysName/ONTOLOGY_LEFT_SIDE_BAR/true/false/true/false
            log.info(
                "Shoot ontology by sys name step \
                 ONTOLOGY_LEFT_SIDE_BAR step: %s", missile)
            try:
                self.left_side_bar_ontology = self.ontology_api_instance.ontology_get_ontology_by_sys_name(
                    sys_name='ONTOLOGY_LEFT_SIDE_BAR', load_attributes=True,
                    load_visualisation=False, load_entities=True, load_binaries=False)
            except ApiException as exc:
                log.error(exc)
                raise

    # Загрузка онтологии TopBar#

    def case_3(self, missile):
        with self.gun.measure("case_3"):
            # GET
            # <BASE_API_URL>/api/Ontology/ontologyBySysName/ONTOLOGY_TOP_BAR/true/false/true/false
            log.info(
                "Shoot ontology by sys name step \
                 ONTOLOGY_TOP_BAR step: %s", missile)
            try:
                self.top_bar_ontology = self.ontology_api_instance.ontology_get_ontology_by_sys_name(
                    sys_name='ONTOLOGY_TOP_BAR', load_attributes=True, load_visualisation=False, load_entities=True, load_binaries=False)
            except ApiException as exc:
                log.error(exc)
                raise

    # Загрузка профиля пользователя из ISAPI#

    def case_4(self, missile):
        with self.gun.measure("case_4"):
            # GET <BASE_ISAPI_URL>/api/v1/profile?email=<USER_NAME>
            log.info("Shoot get user profile from isapi step: %s", missile)
            try:
                if self.base_isapi_url:
                    headers = {
                        'accept': 'application/json',
                        'Authorization': 'Bearer {}'.format(self.access_token), }
                    params = (('email', self.login),)
                    requests.get(
                        '{0}/api/v1/profile'.format(self.base_isapi_url), headers=headers, params=params)
            except ApiException as exc:
                log.error(exc)
                raise

    # Возвращает идентификатор справочника онтологий#

    def case_5(self, missile):
        with self.gun.measure("case_5"):
            # GET <BASE_API_URL>/api/Ontology
            # return <ONTOLOGY_OF_ONTOLOGIES_ID>
            log.info("Shoot get ontology id case step: %s", missile)
            try:
                self.ontology_of_ontologies_id = self.ontology_api_instance.ontology_get_main_ontology_id()
                if self.ontology_of_ontologies_id is None:
                    raise ApiException(
                        'case_5. Shoot failed step: ontology_of_ontologies_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получения сущностей из справочника онтологий#

    def case_6(self, missile):
        with self.gun.measure("case_6"):
            # GET
            # <BASE_API_URL>/api/Ontology/<ONTOLOGY_OF_ONTOLOGIES_ID>/true/true/true/false
            log.info(
                "Shoot retrieving entities from the ontology reference step: %s", missile)
            try:
                if self.ontology_of_ontologies_id:
                    self.ontology_api_instance.ontology_get(
                        id=self.ontology_of_ontologies_id, load_attributes=True, load_visualisation=True, load_entities=True, load_binaries=False)
            except ApiException as exc:
                log.error(exc)
                raise

    # Получения справочника "Канбан панели"#

    def case_7(self, missile):
        with self.gun.measure("case_7"):
            # GET
            # <BASE_API_URL>/api/Ontology/<KANBAN_PANEL_ONTOLOGY_ID>/true/true/true/false
            log.info("Shoot getting kanban panel reference step: %s", missile)
            try:
                if self.ontology_of_ontologies:
                    if self.kanban_panel_id:
                        self.kanban_panel_ontology = self.ontology_api_instance.ontology_get(
                            id=self.kanban_panel_id, load_attributes=True, load_visualisation=True, load_entities=True, load_binaries=False)
                    else:
                        raise ApiException(
                            'case_7. Shoot failed step: kanban_panel_id is None')
                else:
                    raise ApiException(
                        "Shoot failed step")
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение сущности Справочник онтологий #

    def case_8(self, missile):
        with self.gun.measure("case_8"):
            # GET <BASE_API_URL>/api/OntologyEntity/<ONTOLOGY_OF_ONTOLOGIES_ID>
            log.info("Shoot get entity ontology reference step: %s", missile)
            try:
                if self.ontology_of_ontologies_id:
                    self.ontology_of_ontologies_entity = self.ontology_entity_api_instance.ontology_entity_get(
                        id=self.ontology_of_ontologies_id)
                else:
                    raise ApiException(
                        'case_8. Shoot failed step: ontology_of_ontologies_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение сущностей справочника "Представление по умолчанию"#

    def case_9(self, missile):
        with self.gun.measure("case_9"):
            # GET
            # <BASE_API_URL>/api/Ontology/<DEFAULT_VIEW_ONTOLOGY_ID>/true/true/true/false
            log.info(
                "Shoot getting the entities of the \"Default view\" reference step: %s", missile)
            try:
                if self.default_view_ontology_id:
                    self.default_view_ontology = self.ontology_api_instance.ontology_get(
                        id=self.default_view_ontology_id, load_attributes=True, load_visualisation=True, load_entities=True, load_binaries=False)
                else:
                    raise ApiException(
                        'case_9. Shoot failed step: default_view_ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение сущностей справочника "Оборудование"#

    def case_10(self, missile):
        with self.gun.measure("case_10"):
            # GET
            # <BASE_API_URL>/api/Ontology/<EQUIPMENT_ONTOLOGY_ID>/true/true/true/true
            log.info(
                "Shoot getting the entities of the \"Equipment\" reference step: %s", missile)
            try:
                if self.equipment_ontology_id:
                    self.ontology_api_instance.ontology_get(
                        id=self.equipment_ontology_id, load_attributes=True, load_visualisation=True, load_entities=True, load_binaries=True)
                else:
                    raise ApiException(
                        'case_10. Shoot failed step: equipment_ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение сущностей справочника "Столбцы канбана"#

    def case_11(self, missile):
        with self.gun.measure("case_11"):
            # GET
            # <BASE_API_URL>/api/Ontology/<KANBAN_COLUMNS_ONTOLOGY_ID>/true/true/true/false
            log.info(
                "Shoot getting the entities of \
                 the \"Kanban Columns\" reference step: %s", missile)
            try:
                if self.kanban_columns_ontology_id:
                    self.ontology_api_instance.ontology_get(
                        id=self.kanban_columns_ontology_id, load_attributes=True, load_visualisation=True, load_entities=True, load_binaries=True)
                else:
                    raise ApiException(
                        'case_11. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение сущностей справочника "Состояния"#

    def case_12(self, missile):
        with self.gun.measure("case_12"):
            # GET
            # <BASE_API_URL>/api/Ontology/<STATES_ONTOLOGY_ID>/true/true/true/false
            log.info(
                "Shoot getting the entities of the \"States\" reference step: %s", missile)
            try:
                if self.states_ontology_id:
                    self.ontology_api_instance.ontology_get(
                        id=self.states_ontology_id, load_attributes=True, load_visualisation=True, load_entities=True, load_binaries=False)
                else:
                    raise ApiException(
                        'case_12. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Top menu selection cases #
    # Выбор верхнего меню (Сменно-суточные задания)#

    # Получение сущностей справочника "Сменно-суточные задания"#

    def case_13(self, missile):
        with self.gun.measure("case_13"):
            # GET
            # <BASE_API_URL>/api/Ontology/SHIFT_DAILY_TASKS_ONTOLOGY_ID/true/true/true/false
            log.info(
                "Shoot getting the entities of \
                the \"Shift-daily tasks\" reference step: %s", missile)
            try:
                if self.shift_daily_tasks_ontology_id:
                    self.ontology_api_instance.ontology_get(
                        id=self.shift_daily_tasks_ontology_id, load_attributes=True, load_visualisation=True, load_entities=True, load_binaries=False)
                else:
                    raise ApiException(
                        'case_13. Shoot failed step: shift_daily_tasks_ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Загрузка онтологии LeftSideBar#

    def case_14(self, missile):
        with self.gun.measure("case_14"):
            # GET
            # <BASE_API_URL>/api/Ontology/ontologyBySysName/ONTOLOGY_LEFT_SIDE_BAR/true/false/true/false
            log.info(
                "Shoot getting ontology by sys name step \
                ONTOLOGY_LEFT_SIDE_BAR step: %s", missile)
            try:
                self.left_side_bar_ontology = self.ontology_api_instance.ontology_get_ontology_by_sys_name(
                    sys_name='ONTOLOGY_LEFT_SIDE_BAR', load_attributes=True, load_visualisation=False, load_entities=True, load_binaries=False)
            except ApiException as exc:
                log.error(exc)
                raise

    # Загрузка онтологии TopBar#

    def case_15(self, missile):
        with self.gun.measure("case_15"):
            # GET
            # <BASE_API_URL>/api/Ontology/ontologyBySysName/ONTOLOGY_TOP_BAR/true/false/true/false
            log.info(
                "Shoot getting ontology by sys name step ONTOLOGY_TOP_BAR step: %s", missile)
            try:
                self.top_bar_ontology = self.ontology_api_instance.ontology_get_ontology_by_sys_name(
                    sys_name='ONTOLOGY_TOP_BAR', load_attributes=True, load_visualisation=False, load_entities=True, load_binaries=False)
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Сотрудники"#

    def case_16(self, missile):
        with self.gun.measure("case_16"):
            # GET
            # <BASE_API_URL>/api/Ontology/<EMPLOYEES_ONTOLOGY_ID>/Light/ru
            log.info(
                "Shoot getting a simple list of entities \
                from the \"Employees\" ontology step: %s", missile)
            try:
                if self.employees_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.employees_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_16. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение сущности БП  для онтологии "Сменно-суточные задания"#

    def case_17(self, missile):
        with self.gun.measure("case_17"):
            # GET
            # <BASE_API_URL>/api/OntologyEntity/<BP_ENTITY_ID>
            log.info(
                "Shoot getting BP entity for \
                the \"Shift-daily tasks\" ontology step: %s", missile)
            try:
                if self.shift_daily_tasks_ontology:
                    if self.bp_enity_id:
                        self.ontology_entity_api_instance.ontology_entity_get(
                            id=self.bp_enity_id)
                    else:
                        raise ApiException(
                            'case_17. Shoot failed step: bp_enity_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение сущностей справочника "Сменно-суточные задания"#

    def case_18(self, missile):
        with self.gun.measure("case_18"):
            # GET
            # <BASE_API_URL>/api/Ontology/SHIFT_DAILY_TASKS_ONTOLOGY_ID/true/true/true/false
            log.info(
                "Shoot getting the entities of \
                the \"Shift-daily tasks\" reference step: %s", missile)
            try:
                if self.shift_daily_tasks_ontology_id:
                    self.ontology_api_instance.ontology_get(
                        id=self.shift_daily_tasks_ontology_id, load_attributes=True,
                        load_visualisation=True, load_entities=True, load_binaries=False)
                else:
                    raise ApiException(
                        'case_18. Shoot failed step: shift_daily_tasks_ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Номенклатура"#

    def case_19(self, missile):
        with self.gun.measure("case_19"):
            # GET
            # <BASE_API_URL>/api/Ontology/<NOMENCLATURE_ONTOLOGY_ID>/Light/ru
            log.info(
                "Shoot getting a simple list of entities from \
                the \"Nomenclature\" ontology step: %s", missile)
            try:
                if self.nomenclature_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.nomenclature_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_19. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение сущностей справочника "Сменно-суточные задания"#

    def case_20(self, missile):
        with self.gun.measure("case_20"):
            # GET
            # <BASE_API_URL>/api/Ontology/SHIFT_DAILY_TASKS_ONTOLOGY_ID/true/true/true/false
            log.info(
                "Shoot getting the entities of \
                the \"Shift-daily tasks\" reference step: %s", missile)
            try:
                if self.shift_daily_tasks_ontology_id:
                    self.ontology_api_instance.ontology_get(
                        id=self.shift_daily_tasks_ontology_id, load_attributes=True,
                        load_visualisation=True, load_entities=True, load_binaries=False)
                else:
                    raise ApiException(
                        'case_20. Shoot failed step: shift_daily_tasks_ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Select card on kanban panel cases#
    # Выбор карточки на канбан панели#

    # Получение сущности выбранной карточки на канбан-панели#

    def case_21(self, missile):
        with self.gun.measure("case_21"):
            # GET
            # <BASE_API_URL>/api/OntologyEntity/<SELECTED_CARD_ENTITY_ID>
            log.info(
                "Shoot getting the entity of the selected card \
                on the kanban panel step: %s", missile)
            try:
                if self.selected_card_entity_id:
                    self.selected_card_entity = self.ontology_entity_api_instance.ontology_entity_get(
                        id=self.selected_card_entity_id)
                else:
                    raise ApiException(
                        'case_21. Shoot failed step: self.selected_card_entity_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Загрузка визуализации для онтологии выбранной сущности #

    def case_22(self, missile):
        with self.gun.measure("case_22"):
            # GET
            # <BASE_API_URL>/api/Ontology/<ONTOLOGY_ID>/true/true/false/false
            log.info(
                "Shoot loading visualization for the ontology \
                of the selected entity step: %s", missile)
            try:
                if self.selected_ontology_id:
                    self.ontology_api_instance.ontology_get(
                        id=self.selected_ontology_id, load_attributes=True,
                        load_visualisation=True, load_entities=False, load_binaries=False)
                else:
                    raise ApiException(
                        'case_22. Shoot failed step: selected_ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Возвращает визуализацию онтологии с правами для текущего пользователя#

    def case_23(self, missile):
        with self.gun.measure("case_23"):
            # GET
            # <BASE_API_URL>/api/SecurityAdministration/<ONTOLOGY_ID>/permissionsForUser/CURRENT_USER_ID
            log.info(
                "Shoot returns a visualization of the ontology \
                with rights for the current user step: %s", missile)
            try:
                if self.selected_ontology_id:
                    self.security_api_instance.security_administration_get_ontology_user_permissions(
                        user_id=self.user_id, ontology_id=self.selected_ontology_id)
                else:
                    raise ApiException(
                        'case_23. Shoot failed step: selected_ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получает формат вывода для выбранной сущности #

    def case_24(self, missile):
        with self.gun.measure("case_24"):
            # GET
            # <BASE_API_URL>/api/OntologyEntity/<ONTOLOGY_ID>/<ENTITY_ID>/Light/ru-RU
            log.info(
                "Shoot gets the output format for the selected \
                entity step: %s", missile)
            try:
                if self.selected_ontology_id and self.selected_card_entity_id:
                    self.ontology_entity_api_instance.ontology_entity_get_light(
                        ontology_id=self.selected_ontology_id, id=self.selected_card_entity_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_24. Shoot failed step: selected_ontology_id is None or selected_card_entity_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Тип задания"#

    def case_25(self, missile):
        with self.gun.measure("case_25"):
            # GET
            # <BASE_API_URL>/api/Ontology/<JOB_TYPE_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                the \"Job type\" ontology step: %s", missile)
            try:
                if self.job_type_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.job_type_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_25. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Сотрудники"#

    def case_26(self, missile):
        with self.gun.measure("case_26"):
            # GET
            # <BASE_API_URL>/api/Ontology/<EMPLOYEES_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                the \"Employees\" ontology step: %s", missile)
            try:
                if self.employees_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.employees_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_26. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Состояния"#

    def case_27(self, missile):
        with self.gun.measure("case_27"):
            # GET
            # <BASE_API_URL>/api/Ontology/<STATES_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                the \"States\" ontology step: %s", missile)
            try:
                if self.states_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.states_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_27. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Номенклатура"#

    def case_28(self, missile):
        with self.gun.measure("case_28"):
            # GET
            # <BASE_API_URL>/api/Ontology/<NOMENCLATURE_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                the \"Nomenclature\" ontology step: %s", missile)
            try:
                if self.nomenclature_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.nomenclature_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_28. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Технологические маршруты"#

    def case_29(self, missile):
        with self.gun.measure("case_29"):
            # GET
            # <BASE_API_URL>/api/Ontology/<TECHNOLOGICAL_ROUTES_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                the \"Technological Routes\" ontology step: %s", missile)
            try:
                if self.routes_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.routes_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_29. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Сотрудники"#

    def case_30(self, missile):
        with self.gun.measure("case_30"):
            # GET
            # <BASE_API_URL>/api/Ontology/<EMPLOYEES_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                the \"Employees\" ontology step: %s", missile)
            try:
                if self.employees_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.employees_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_30. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Оборудование"#

    def case_31(self, missile):
        with self.gun.measure("case_31"):
            # GET
            # <BASE_API_URL>/api/Ontology/<EQUIPMENT_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                the \"Equipment\" ontology step: %s", missile)
            try:
                if self.equipment_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.equipment_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_31. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Причина брака"#

    def case_32(self, missile):
        with self.gun.measure("case_32"):
            # GET
            # <BASE_API_URL>/api/Ontology/<REASON_OF_DEFECT_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                the \"Reason for defect\" ontology step: %s", missile)
            try:
                if self.defects_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.defects_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_32. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Резолюции"#

    def case_33(self, missile):
        with self.gun.measure("case_33"):
            # GET
            # <BASE_API_URL>/api/Ontology/RESOLUTIONS_ONTOLOGY_ID/Light/ru-RU
            log.info(
                "Getting a simple list of entities from \
                the \"Resolutions\" ontology step: %s", missile)
            try:
                if self.resolutions_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.resolutions_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_33. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Saving entity cases / Сохранение сущности#

    # Сохранение сущности #

    def case_34(self, missile):
        with self.gun.measure("case_34"):
            # POST
            # <BASE_API_URL>/api/OntologyEntity
            log.info(
                "Shoot saving entity step: %s", missile)
            try:
                if self.selected_card_entity:
                    self.ontology_entity_api_instance.ontology_entity_post(
                        body=self.selected_card_entity)
                else:
                    raise ApiException(
                        'case_34. Shoot failed step: selected_card_entity is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получает формат вывода для выбранной сущности #

    def case_35(self, missile):
        with self.gun.measure("case_35"):
            # GET
            # <BASE_API_URL>/api/OntologyEntity/<ONTOLOGY_ID>/<SELECTED_ENTITY_ID>/Light/ru-RU
            log.info(
                "Shoot Gets the output format for the selected entity step: %s", missile)
            try:
                if self.selected_ontology_id and self.selected_card_entity_id:
                    self.ontology_entity_api_instance.ontology_entity_get_light(
                        ontology_id=self.selected_ontology_id, id=self.selected_card_entity_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_35. Shoot failed step: selected_ontology_id is None or selected_card_entity_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Сотрудники"#

    def case_36(self, missile):
        with self.gun.measure("case_36"):
            # GET
            # <BASE_API_URL>/api/Ontology/<EMPLOYEES_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                the \"Employees\" ontology step: %s", missile)
            try:
                if self.employees_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.employees_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_36. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Номенклатура"#

    def case_37(self, missile):
        with self.gun.measure("case_37"):
            # GET
            # <BASE_API_URL>/api/Ontology/<NOMENCLATURE_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                the \"Nomenclature\" ontology step: %s", missile)
            try:
                if self.nomenclature_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.nomenclature_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_37. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение сущности БП  для онтологии "Сменно-суточные задания"#

    def case_38(self, missile):
        with self.gun.measure("case_38"):
            # GET
            # <BASE_API_URL>/api/OntologyEntity/<SHIFT_DAILY_TASKS_ONTOLOGY_ID>
            log.info(
                "Shoot Obtaining the BP entity for \
                the \"Shift-daily tasks\" ontology step: %s", missile)
            try:
                if self.shift_daily_tasks_ontology:
                    if self.bp_enity_id:
                        self.ontology_entity_api_instance.ontology_entity_get(
                            id=self.bp_enity_id)
                    else:
                        raise ApiException(
                            'case_38. Shoot failed step: bp_enity_id is None')
                else:
                    raise ApiException(
                        'case_38. Shoot failed step: shift_daily_tasks_ontology is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Тип задания"#

    def case_39(self, missile):
        with self.gun.measure("case_39"):
            # GET
            # <BASE_API_URL>/api/Ontology/<JOB_TYPE_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                the \"Job type\" ontology step: %s", missile)
            try:
                if self.job_type_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.job_type_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_39. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Сотрудники"#

    def case_40(self, missile):
        with self.gun.measure("case_40"):
            # GET
            # <BASE_API_URL>/api/Ontology/<EMPLOYEES_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                the \"Employees\" ontology step: %s", missile)
            try:
                if self.employees_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.employees_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_40. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Состояния"#

    def case_41(self, missile):
        with self.gun.measure("case_41"):
            # GET
            # <BASE_API_URL>/api/Ontology/<STATES_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                 the \"States\" ontology step: %s", missile)
            try:
                if self.states_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.states_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_41. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Номенклатура"#

    def case_42(self, missile):
        with self.gun.measure("case_42"):
            # GET
            # <BASE_API_URL>/api/Ontology/<NOMENCLATURE_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                 the \"Nomenclature\" ontology step: %s", missile)
            try:
                if self.nomenclature_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.nomenclature_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_42. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Технологические маршруты"#

    def case_43(self, missile):
        with self.gun.measure("case_43"):
            # GET
            # <BASE_API_URL>/api/Ontology/<TECHNOLOGICAL_ROUTES_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                 the \"Technological Routes\" ontology step: %s", missile)
            try:
                if self.routes_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.routes_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_43. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Сотрудники"#

    def case_44(self, missile):
        with self.gun.measure("case_44"):
            # GET <BASE_API_URL>/api/Ontology/<EMPLOYEES_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                 the \"Employees\" ontology step: %s", missile)
            try:
                if self.employees_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.employees_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_44. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Оборудование"#

    def case_45(self, missile):
        with self.gun.measure("case_45"):
            # GET <BASE_API_URL>/api/Ontology/<EQUIPMENT_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                 the \"Equipment\" ontology step: %s", missile)
            try:
                if self.equipment_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.equipment_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_45. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Причина брака"#

    def case_46(self, missile):
        with self.gun.measure("case_46"):
            # GET
            # <BASE_API_URL>/api/Ontology/<REASON_FOR_DEFECT_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                 the \"Reason for defect\" ontology step: %s", missile)
            try:
                if self.defects_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.defects_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_46. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Резолюции"#

    def case_47(self, missile):
        with self.gun.measure("case_47"):
            # GET <BASE_API_URL>/api/Ontology/<RESOLUTIONS_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                 the \"Resolutions\" ontology step: %s", missile)
            try:
                if self.resolutions_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.resolutions_ontology_id, culture_name='ru-RU')
                else:
                    raise ApiException(
                        'case_47. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение сущности выбранной карточки на канбан-панели#

    def case_48(self, missile):
        with self.gun.measure("case_48"):
            # GET <BASE_API_URL>/api/OntologyEntity/<SELECTED_CARD_ENTITY_ID>
            log.info(
                "Shoot Getting the entity of the selected card \
                 on the kanban panel step: %s", missile)
            try:
                if self.selected_ontology_id:
                    self.ontology_entity_api_instance.ontology_entity_get(
                        id=self.selected_ontology_id)
                else:
                    raise ApiException(
                        'case_48. Shoot failed step: selected_ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Сотрудники"#

    def case_49(self, missile):
        with self.gun.measure("case_49"):
            # GET <BASE_API_URL>/api/Ontology/<EMPLOYEES_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities from \
                 the \"Employees\" ontology step: %s", missile)
            try:
                if self.employees_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.employees_ontology_id, culture_name='ru-RU')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение простого списка сущностей из онтологии "Номенклатура"#

    def case_50(self, missile):
        with self.gun.measure("case_50"):
            # GET <BASE_API_URL>/api/Ontology/<NOMENCLATURE_ONTOLOGY_ID>/Light/ru-RU
            log.info(
                "Shoot Getting a simple list of entities \
                 from the \"Nomenclature\" ontology step: %s", missile)
            try:
                if self.nomenclature_ontology_id:
                    self.ontology_api_instance.ontology_get_light(
                        id=self.nomenclature_ontology_id, culture_name='ru-RU')

                else:
                    raise ApiException(
                        'case_50. Shoot failed step: ontology_id is None')
            except ApiException as exc:
                log.error(exc)
                raise

    # Получение сущности БП  для онтологии "Сменно-суточные задания"#

    def case_51(self, missile):
        with self.gun.measure("case_51"):
            # GET <BASE_API_URL>/api/OntologyEntity/<BP_ENTITY_ID>
            log.info(
                "Shoot Obtaining the BP entity for \
                 the \"Shift-daily tasks\" ontology step: %s", missile)
            try:
                if self.shift_daily_tasks_ontology:
                    if self.bp_enity_id:
                        self.ontology_entity_api_instance.ontology_entity_get(
                            id=self.bp_enity_id)
                    else:
                        raise ApiException(
                            'case_51. Shoot failed step: bp_enity_id is None')
                else:
                    raise ApiException(
                        'case_51. Shoot failed step: shift_daily_tasks_ontology is None')

            except ApiException as exc:
                log.error(exc)
                raise

    def setup(self, param):
        # this will be executed in each worker before the test starts #
        log.info("Setting up LoadTest with paramv value {0}".format(param))

    def teardown(self):
        # this will be executed in each worker after the end of the test #
        log.info("Tearing down LoadTest")
        # It's mandatory to explicitly stop worker process in teardown
        # pylint: disable=protected-access
        os._exit(0)
        return 0
