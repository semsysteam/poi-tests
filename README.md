# Использование

## установить зависимости

        `sudo apt update && sudo apt-get install software-properties-common python3-pip build-essential python3-dev libffi-dev gfortran libssl-dev`
        `sudo add-apt-repository ppa:deadsnakes/ppa`
        Нажать клавишу Enter для продолжения при запросе:
        `Press [ENTER] to continue or Ctrl-c to cancel adding it.`

Установить python3.7

        `sudo apt install python3.7`

Обновить pip:

        `pip install --upgrade pip`

Установить или обновить setuptools:

        `pip3 install --upgrade setuptools`

Установить свежий билд Yandex.Tank из ветки master:

        `pip3 install https://api.github.com/repos/yandex/yandex-tank/tarball/master`

Для текущего проекта

        `pip3 install -r requirements.txt`
       
Для  python-api-client 
    
<b>ВНИМАНИЕ</b> необходимо будет ввести учетные данные для https://bitbucket.org/semsysteam

        `pip3 install git+https://bitbucket.org/semsysteam/python-api-client.git`


В каталоге проекта создать файлы .env и token.txt

        `cat <<EOF > ".env"
BASE_API_URL=
BASE_ISAPI_URL=
API_USER_NAME=
API_USER_PASSWORD=
EOF`

`cat <<EOF > "token.txt"
overload_token_value
EOF`


## Запуск с параметрами:

В текущем каталоге проекта выполнить

        `yandex-tank -c load.yaml -q`
